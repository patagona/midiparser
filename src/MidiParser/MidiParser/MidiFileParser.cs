﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MidiParser.Events;
using MidiParser.Events.MetaEvents;
using MidiParser.Events.MidiEvents;
using MidiParser.Unsafe;

namespace MidiParser
{
	// https://www.csie.ntu.edu.tw/~r92092/ref/midi/
	// http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/midispec.htm
	// http://www.ccarh.org/courses/253/handout/smf/
	// http://faydoc.tripod.com/formats/mid.htm

	public class MidiFileParser
	{
		public MidiFile Parse(Stream stream)
		{
			var header = new byte[] {0x4D, 0x54, 0x68, 0x64};

			for (var i = 0; i < 4; i++)
			{
				if (stream.Get<byte>() != header[i])
					return null;
			}

			var headerLength = stream.Get<uint>();

			if (headerLength != 6)
				return null;

			var toReturn = new MidiFile();

			var format = stream.Get<MidiFormat>();

			toReturn.Format = format;
			
			var chunkCount = stream.Get<ushort>();

			var division = stream.Get<short>();
			if (division < 0)
				throw new NotImplementedException("negative division");

			toReturn.TicksPerQuarterNote = division;
			var tracks = new List<MidiTrack>();

			for (var i = 0; i < chunkCount; i++)
			{
				tracks.Add(ParseTrack(stream));
			}

			if(format == MidiFormat.SingleTrack && chunkCount != 1)
				throw new ArgumentException("single track file with multiple chunks?", "stream");

			toReturn.Songs = new List<MidiSong>();

			switch (format)
			{
				case MidiFormat.SingleTrack:
					toReturn.Songs.Add(new MidiSong
					{
						Tracks = new[] {tracks[0]}
					});
					break;
				case MidiFormat.MultipleTrack:
					toReturn.Songs.Add(new MidiSong
					{
						Tracks = tracks
					});
					break;
				case MidiFormat.MultipleSong:
					foreach (var track in tracks)
					{
						toReturn.Songs.Add(new MidiSong
						{
							Tracks = new[] {track}
						});
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return toReturn;
		}

		private MidiTrack ParseTrack(Stream stream)
		{
			var header = new byte[] {0x4D, 0x54, 0x72, 0x6B};

			for (var i = 0; i < 4; i++)
			{
				if (stream.Get<byte>() != header[i])
					return null;
			}

			var length = stream.Get<uint>();

			var toReturn = new MidiTrack();
			toReturn.Events = new List<TrackEvent>();
			var position = 0;

			while (position < length)
			{
				toReturn.Events.Add(ParseEvent(stream, ref position));
			}

			return toReturn;
		}

		private TrackEvent ParseEvent(Stream stream, ref int position)
		{
			var deltaTime = stream.GetVariable(ref position);

			var type = stream.Get<byte>(ref position);

			if ((type & (1 << 7)) == 0)
				throw new ArgumentException(string.Format("Not a message start: 0x{0:X2}", type));

			TrackEvent toReturn;

			if (type >= 0x80 && type <= 0xEF)
			{
				toReturn = ParseMidiEvent(stream, type, ref position);
			}
			else if (type == 0xFF)
			{
				toReturn = ParseMetaEvent(stream, ref position);
			}
			else if (type == 0xF0)
			{
				toReturn = ParseSysExEvent(stream, ref position);
			}
			else
			{
				throw new NotImplementedException(string.Format("Unhandled Midi Event 0x{0:X2}", type));
			}
			toReturn.DeltaTime = deltaTime;

			return toReturn;
		}

		private MidiEvent ParseMidiEvent(Stream stream, byte type, ref int position)
		{
			const byte commandBitMask = 0xF0;
			const byte channelBitMask = 0x0F;

			var command = (byte)(type & commandBitMask);

			MidiEvent toReturn;

			switch (command)
			{
				case 0x80:
					toReturn = new NoteOffEvent
					{
						Note = new NoteInfo(stream.Get<byte>(ref position)),
						Velocity = stream.Get<byte>(ref position)
					};
					break;
				case 0x90:
					toReturn = new NoteOnEvent
					{
						Note = new NoteInfo(stream.Get<byte>(ref position)),
						Velocity = stream.Get<byte>(ref position)
					};
					break;
				case 0xA0:
					toReturn = new KeyAfterTouchEvent
					{
						Note = new NoteInfo(stream.Get<byte>(ref position)),
						Pressure = stream.Get<byte>(ref position)
					};
					break;
				case 0xB0:
					toReturn = new ControllerChange
					{
						ControllerNumber = stream.Get<byte>(ref position),
						Value = stream.Get<byte>(ref position)
					};
					break;
				case 0xC0:
					toReturn = new ProgramChange
					{
						ProgramNumber = stream.Get<byte>(ref position)
					};
					break;
				case 0xD0:
					toReturn = new ChannelPressure
					{
						Pressure = stream.Get<byte>(ref position)
					};
					break;
				case 0xE0:
					toReturn = new PitchWheelChange
					{
						Value = stream.Get14Bit(ref position)
					};
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			toReturn.Channel = (byte)(type & channelBitMask);
			return toReturn;
		}

		private MetaEvent ParseMetaEvent(Stream stream, ref int position)
		{
			var eventType = stream.Get<MetaEventType>(ref position);

			if (!Enum.IsDefined(typeof (MetaEventType), eventType))
				throw new NotSupportedException(string.Format("Unsupported Meta Event: 0x{0:X2}", (byte) eventType));

			MetaEvent toReturn;

			switch (eventType)
			{
				case MetaEventType.SequenceNumber:
					if(stream.Get<byte>(ref position) != 0x02)
						throw new NotSupportedException("WTF? SequenceNumber high bytes should be 0x02");
					toReturn = new SequenceNumber
					{
						Number = stream.Get<ushort>(ref position)
					};
					break;
				case MetaEventType.TextEvent:
				case MetaEventType.CopyrightNotice:
				case MetaEventType.SequenceOrTrackName:
				case MetaEventType.InstrumentName:
				case MetaEventType.LyricText:
				case MetaEventType.MarkerText:
				case MetaEventType.CuePoint:
				{
					var length = stream.GetVariable(ref position);
					var buffer = new byte[length];
					stream.Read(buffer, 0, length);
					position += length;
					toReturn = new TextEvent
					{
						Type = eventType,
						Text = Encoding.ASCII.GetString(buffer)
					};
					break;
				}
				case MetaEventType.MidiChannelPrefixAssignment:
					if (stream.Get<byte>(ref position) != 0x01)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new MidiChannelPrefixAssignment
					{
						Channel = stream.Get<byte>(ref position)
					};
					break;
				case MetaEventType.EndOfTrack:
					if (stream.Get<byte>(ref position) != 0x00)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new EndOfTrackEvent();
					break;
				case MetaEventType.SetTempo:
					if (stream.Get<byte>(ref position) != 0x03)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new SetTempoEvent
					{
						Tempo = stream.Get24Bit(ref position)
					};
					break;
				case MetaEventType.SmpteOffset:
					if (stream.Get<byte>(ref position) != 0x05)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new SmtpeOffset
					{
						Hours = stream.Get<byte>(ref position),
						Minutes = stream.Get<byte>(ref position),
						Seconds = stream.Get<byte>(ref position),
						Frames = stream.Get<byte>(ref position),
						FrameFractions = stream.Get<byte>(ref position)
					};
					break;
				case MetaEventType.TimeSignature:
					if (stream.Get<byte>(ref position) != 0x04)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new TimeSignature
					{
						Numerator = stream.Get<byte>(ref position),
						Denominator = stream.Get<byte>(ref position),
						TicksPerMetronomeTick = stream.Get<byte>(ref position),
						One32NdsPerQuarterMidiNote = stream.Get<byte>(ref position)
					};
					break;
				case MetaEventType.KeySignature:
					if (stream.Get<byte>(ref position) != 0x02)
						throw new FormatException("Wrong Meta Message Length");
					toReturn = new KeySignature
					{
						FlatsSharps = stream.Get<sbyte>(ref position),
						MajorMinor = stream.Get<MajorMinor>(ref position)
					};
					break;
				case MetaEventType.SequencerSpecific:
				{
					var length = stream.GetVariable(ref position);
					var buffer = new byte[length];
					stream.Read(buffer, 0, length);
					position += length;
					toReturn = new SequencerSpecific
					{
						Bytes = buffer
					};
					break;
				}
				default:
					throw new ArgumentOutOfRangeException();
			}

			return toReturn;
		}

		private TrackEvent ParseSysExEvent(Stream stream, ref int position)
		{
			TrackEvent toReturn;
			var sysExEvent = new SysExEvent();
			using (var ms = new MemoryStream())
			{
				byte val;
				while ((val = stream.Get<byte>(ref position)) != 0xF7)
				{
					ms.WriteByte(val);
				}

				sysExEvent.Bytes = ms.ToArray();
			}

			toReturn = sysExEvent;
			return toReturn;
		}
	}
}
