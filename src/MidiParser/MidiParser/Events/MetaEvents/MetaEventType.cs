namespace MidiParser.Events.MetaEvents
{
	public enum MetaEventType : byte
	{
		SequenceNumber = 0x00,
		TextEvent = 0x01,
		CopyrightNotice = 0x02,
		SequenceOrTrackName = 0x03,
		InstrumentName = 0x04,
		LyricText = 0x05,
		MarkerText = 0x06,
		CuePoint = 0x07,
		MidiChannelPrefixAssignment = 0x20,
		EndOfTrack = 0x2F,
		SetTempo = 0x51,
		SmpteOffset = 0x54,
		TimeSignature = 0x58,
		KeySignature = 0x59,
		SequencerSpecific = 0x7F
	}
}