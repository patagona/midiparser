namespace MidiParser.Events.MetaEvents
{
	public class TimeSignature : MetaEvent
	{
		public byte Numerator { get; set; }
		public byte Denominator { get; set; }
		public byte TicksPerMetronomeTick { get; set; }
		public byte One32NdsPerQuarterMidiNote { get; set; }
	}
}