namespace MidiParser.Events.MetaEvents
{
	public class SequenceNumber : MetaEvent
	{
		public ushort Number { get; set; }
	}
}