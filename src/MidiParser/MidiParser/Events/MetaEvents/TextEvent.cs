namespace MidiParser.Events.MetaEvents
{
	public class TextEvent : MetaEvent
	{
		public MetaEventType Type { get; set; }
		public string Text { get; set; }
	}
}