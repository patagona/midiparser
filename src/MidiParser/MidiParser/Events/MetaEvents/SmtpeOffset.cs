namespace MidiParser.Events.MetaEvents
{
	public class SmtpeOffset : MetaEvent
	{
		public byte Hours { get; set; }
		public byte Minutes { get; set; }
		public byte Seconds { get; set; }
		public byte Frames { get; set; }
		public byte FrameFractions { get; set; }
	}
}