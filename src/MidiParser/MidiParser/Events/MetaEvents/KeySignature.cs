namespace MidiParser.Events.MetaEvents
{
	public class KeySignature : MetaEvent
	{
		public sbyte FlatsSharps { get; set; }
		public MajorMinor MajorMinor { get; set; }
	}
}