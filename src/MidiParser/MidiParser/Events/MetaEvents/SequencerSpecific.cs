namespace MidiParser.Events.MetaEvents
{
	public class SequencerSpecific : MetaEvent
	{
		public byte[] Bytes { get; set; }
	}
}