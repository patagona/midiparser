namespace MidiParser.Events.MetaEvents
{
	public class SetTempoEvent : MetaEvent
	{
		public int Tempo { get; set; }
	}
}