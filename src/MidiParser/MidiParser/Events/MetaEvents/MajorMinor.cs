namespace MidiParser.Events.MetaEvents
{
	public enum MajorMinor : byte
	{
		Major = 0x00,
		Minor = 0x01
	}
}