namespace MidiParser.Events.MetaEvents
{
	public class MidiChannelPrefixAssignment : MetaEvent
	{
		public byte Channel { get; set; }
	}
}