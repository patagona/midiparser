namespace MidiParser.Events
{
	public abstract class TrackEvent
	{
		public int DeltaTime { get; set; }
	}
}