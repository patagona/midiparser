namespace MidiParser.Events
{
	public class SysExEvent : TrackEvent
	{
		public byte[] Bytes { get; set; }
	}
}