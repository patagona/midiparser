namespace MidiParser.Events.MidiEvents
{
	public class KeyAfterTouchEvent : MidiEvent
	{
		public NoteInfo Note { get; set; }
		public byte Pressure { get; set; }
	}
}