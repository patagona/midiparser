namespace MidiParser.Events.MidiEvents
{
	public abstract class MidiEvent : TrackEvent
	{
		public byte Channel { get; set; }
	}
}