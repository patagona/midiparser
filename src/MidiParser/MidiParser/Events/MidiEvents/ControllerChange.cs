namespace MidiParser.Events.MidiEvents
{
	public class ControllerChange : MidiEvent
	{
		public byte ControllerNumber { get; set; }
		public byte Value { get; set; }
	}
}