namespace MidiParser.Events.MidiEvents
{
	public class NoteOnEvent : MidiEvent
	{
		public NoteInfo Note { get; set; }
		public byte Velocity { get; set; }
	}
}