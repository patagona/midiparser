namespace MidiParser.Events.MidiEvents
{
	public class PitchWheelChange : MidiEvent
	{
		public ushort Value { get; set; }
	}
}