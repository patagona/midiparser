namespace MidiParser.Events.MidiEvents
{
	public class ChannelPressure : MidiEvent
	{
		public byte Pressure { get; set; }
	}
}