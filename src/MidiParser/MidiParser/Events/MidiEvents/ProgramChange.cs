namespace MidiParser.Events.MidiEvents
{
	public class ProgramChange : MidiEvent
	{
		public byte ProgramNumber { get; set; }
	}
}