using System.Collections.Generic;
using MidiParser.Events;

namespace MidiParser
{
	public class MidiTrack
	{
		public IList<TrackEvent> Events { get; set; }
	}
}