using System;

namespace MidiParser
{
	public class NoteInfo
	{
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(this, obj)) return true;

			var noteInfo = obj as NoteInfo;
			if (noteInfo == null)
				return false;

			return _note == noteInfo._note;
		}

		public override int GetHashCode()
		{
			return _note.GetHashCode();
		}

		public static bool operator ==(NoteInfo left, NoteInfo right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(NoteInfo left, NoteInfo right)
		{
			return !Equals(left, right);
		}

		private readonly byte _note;

		public NoteInfo(byte note)
		{
			_note = note;
			int rem;
			var div = Math.DivRem(note, 12, out rem);

			Octave = (byte) div;
			Note = (Note) rem;
		}

		public byte Octave { get; set; }
		public Note Note { get; set; }

		public int ToFrequency()
		{
			return (int) (Math.Pow(2d, (_note - 69d)/12d)*440);
		}

		public override string ToString()
		{
			return string.Format("{0}-{1}", Octave, Note);
		}
	}
}