using System.Collections.Generic;

namespace MidiParser
{
	public class MidiSong
	{
		public IList<MidiTrack> Tracks { get; set; }
	}
}