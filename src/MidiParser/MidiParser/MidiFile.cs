using System.Collections.Generic;

namespace MidiParser
{
	public class MidiFile
	{
		public short TicksPerQuarterNote { get; set; }
		public MidiFormat Format { get; set; }
		public IList<MidiSong> Songs { get; set; }
	}
}