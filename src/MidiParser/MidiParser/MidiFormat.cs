namespace MidiParser
{
	public enum MidiFormat : ushort
	{
		SingleTrack = 0,
		MultipleTrack = 1,
		MultipleSong = 2
	}
}