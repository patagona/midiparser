﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace MidiParser.Unsafe
{
	public static class StreamExtensions
	{
		public static T Get<T>(this Stream stream) where T : struct
		{
			var position = 0;
			return Get<T>(stream, ref position);
		}

		public unsafe static T Get<T>(this Stream stream, ref int position) where T : struct
		{
			var type = typeof(T).IsEnum ? typeof(T).GetEnumUnderlyingType() : typeof(T);

			var size = Marshal.SizeOf(type);
			position += size;
			var buffer = new byte[size];
			
			stream.Read(buffer, 0, size);

			if (BitConverter.IsLittleEndian)
				Array.Reverse(buffer);

			fixed (byte* ptr = buffer)
			{
				return (T)Marshal.PtrToStructure(new IntPtr(ptr), type);
			}
		}

		public static int GetVariable(this Stream stream, ref int position)
		{
			const byte highestBit = 1 << 7;
			const byte bitMask = 255 ^ highestBit;

			var toReturn = 0;

			for (var i = 0; i < 4; i++)
			{
				var readByte = stream.ReadByte();
				position++;
				if(readByte == -1)
					throw new EndOfStreamException();

				var val = (byte) readByte;

				toReturn <<= 7;
				toReturn |= val & bitMask;
				if ((val & highestBit) == 0)
					break;
			}

			return toReturn;
		}

		public static ushort Get14Bit(this Stream stream, ref int position)
		{
			const byte highestBit = 1 << 7;
			const byte bitMask = 255 ^ highestBit;

			var lsb = stream.ReadByte();
			if (lsb == -1)
				throw new EndOfStreamException();
			var msb = stream.ReadByte();
			if (msb == -1)
				throw new EndOfStreamException();

			position += 2;

			return (ushort) ((msb & bitMask) << 7 | lsb);
		}

		public static int Get24Bit(this Stream stream, ref int position)
		{
			var toReturn = 0;

			for (var i = 0; i < 3; i++)
			{
				toReturn <<= 8;
				toReturn |= stream.Get<byte>(ref position);
			}

			return toReturn;
		}
	}
}
