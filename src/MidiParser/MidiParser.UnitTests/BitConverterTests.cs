﻿using System.IO;
using MidiParser.Unsafe;
using NUnit.Framework;

namespace MidiParser.UnitTests
{
	[TestFixture]
    public class Tests
    {
	    [Test]
	    public void TestVariableLength()
	    {
		    DoTest(new byte[] {0x7F}, 127, 1);
		    DoTest(new byte[] {0x81, 0x7F}, 255, 2);
		    DoTest(new byte[] {0x82, 0x80, 0x00}, 32768, 3);
	    }

	    private void DoTest(byte[] bytes, int expected, int byteCount)
	    {
		    using (var str = new MemoryStream(bytes))
		    {
			    var readBytes = 0;
			    Assert.AreEqual(expected, str.GetVariable(ref readBytes));
				Assert.AreEqual(byteCount, readBytes);
		    }
	    }

		[Test]
		public void Test14BitConversion()
		{
			using (var str = new MemoryStream(new byte[] {0x00, 0x40, 0x00, 0x00, 0x7F, 0x7F}))
			{
				var pos = 0;
				Assert.AreEqual(8192, str.Get14Bit(ref pos));
				Assert.AreEqual(0, str.Get14Bit(ref pos));
				Assert.AreEqual(16383, str.Get14Bit(ref pos));
			}
		}
    }
}
