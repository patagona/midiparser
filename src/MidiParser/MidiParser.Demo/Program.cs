﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using MidiParser.Events;
using MidiParser.Events.MetaEvents;
using MidiParser.Events.MidiEvents;

namespace MidiParser.Demo
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MidiParser.Demo.Led_Zeppelin_-_Stairway_to_Heaven.mid"))
			{
				var parser = new MidiFileParser();

				var midiFile = parser.Parse(resourceStream);

				var events = new Dictionary<int, IList<TrackEvent>>();

				foreach (var track in midiFile.Songs[0].Tracks)
				{
					var i = 0;
					foreach (var evnt in track.Events)
					{
						i += evnt.DeltaTime;

						if(!events.ContainsKey(i))
							events[i] = new List<TrackEvent>();

						events[i].Add(evnt);
					}
				}

				using (var str = File.OpenWrite("Out.raw"))
				{
					str.SetLength(0);
					const int sampleRate = 44100;
					const int usPerSample = (1000000/44100);
					var channels = new Dictionary<int, List<SineSynth>>();
					for (int i = 0; i < 15; i++)
					{
						channels[i] = new List<SineSynth>();
					}

					int microSecondsPerQuarterNote = 500000;

					foreach (var kvPair in events.OrderBy(x => x.Key))
					{
						foreach (var trackEvent in kvPair.Value)
						{
							if (trackEvent is SetTempoEvent)
							{
								microSecondsPerQuarterNote = ((SetTempoEvent) trackEvent).Tempo;
							}
							var noteOnEvent = trackEvent as NoteOnEvent;
							if (noteOnEvent != null)
							{
								channels[noteOnEvent.Channel].Add(new SineSynth(sampleRate, noteOnEvent.Note.ToFrequency()));
							}
							var noteOffEvent = trackEvent as NoteOffEvent;
							if (noteOffEvent != null)
							{
								channels[noteOffEvent.Channel].RemoveAll(x => x.Frequency == noteOffEvent.Note.ToFrequency());
							}
						}

						var microSeconds = (double)microSecondsPerQuarterNote/(midiFile.TicksPerQuarterNote);

						for (var i = 0; i < usPerSample*microSeconds / (24 / 2); i++)
						{
							long bigVal = 0;
							var oscillators = channels.SelectMany(x => x.Value).ToList();
							foreach (var oscillator in oscillators)
							{
								bigVal += oscillator.GetSample();
							}

							str.WriteByte((byte) (sbyte) (bigVal/(oscillators.Count > 0 ? oscillators.Count : 1)));
						}
					}
				}
			}
		}
	}

	class SineSynth
	{
		private readonly int _sampleRate;
		private readonly int _frequency;
		private const double RadiansPerCircle = Math.PI * 2;

		public SineSynth(int sampleRate, int frequency)
		{
			_sampleRate = sampleRate;
			_frequency = frequency;
		}

		private int _sampleNumber = 0;

		public int Frequency
		{
			get { return _frequency; }
		}

		public sbyte GetSample()
		{
			var samplesPerOscillation = ((double)_sampleRate / _frequency);
			var depthIntoOscillation = (_sampleNumber++ % samplesPerOscillation) / samplesPerOscillation;
			return (sbyte) (Math.Sin(depthIntoOscillation * RadiansPerCircle) * 127);
		}
	}
}
